package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardsPage extends HomePage{

    @FindBy(css = ".x_title>h2")
    private WebElement processesElement;

    public DashboardsPage(WebDriver driver) {
        super(driver);
    }

    public DashboardsPage assertProcessesHeader() {
        Assert.assertEquals(processesElement.getText(), "DEMO PROJECT");

        return this;
    }

    public DashboardsPage assertProcessesUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);

        return this;
    }
}
