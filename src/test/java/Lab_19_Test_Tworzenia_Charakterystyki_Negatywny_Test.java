import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Lab_19_Test_Tworzenia_Charakterystyki_Negatywny_Test extends SeleniumBaseTest {

    @Test
    public void addCharacteristicNegative() {
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .typeName(characteristicName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreateWithFailure()
                .assertProcessError("The value 'Select process' is not valid for ProjectId.")
                .backToList()
                .assertProcessIsNotShown(characteristicName);


    }
}
