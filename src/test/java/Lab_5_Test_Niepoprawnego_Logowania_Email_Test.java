import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_5_Test_Niepoprawnego_Logowania_Email_Test {

    @Test
    public void incorrectLoginTestWrongEmail() {
        System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.manage().window().maximize();

        driver.get("http://localhost:4444/");


        WebElement emailTxt = driver.findElement(By.cssSelector("input[type='email']"));
        emailTxt.sendKeys("test");

        WebElement passwordTxt = driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys("Test1!");

        WebElement loginButton = driver.findElement(By.cssSelector("button[type='submit']"));
        loginButton.click();

        WebElement emailError = driver.findElement(By.cssSelector("#Email-error"));
        Assert.assertTrue(emailError.getText().contains("The Email field is not a valid e-mail address."));

        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));
        boolean doesErrorExists = validationErrors
                .stream()
                .anyMatch(validationError -> validationError.getText().equals("The Email field is not a valid e-mail address."));
        Assert.assertTrue(doesErrorExists);



        //lista
        List<WebElement> validationErrors2 = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));
        boolean doesErrorExists2 = false;

        for (int i=0; i<validationErrors.size(); i++){
            if (validationErrors2.get(i).getText().equals("The Email field is not a valid e-mail address.")){
                doesErrorExists2 = true;
                break;
            }
        }
        Assert.assertTrue(doesErrorExists2);

        driver.quit();



    }
}
