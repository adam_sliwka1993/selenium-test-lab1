import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_11_Test_Niepoprawnej_Rejestracji_Email extends SeleniumBaseTest{


    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectRegister(String wrongEmail) {

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(wrongEmail);
        createAccountPage.registerWithFailure();
        createAccountPage.assertEmailErrorIsShown();

    }


    @Test(dataProvider = "getWrongEmails")
    public void correctLoginTestWithChaining (String wrongEmail)
    {
        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail(wrongEmail)
                .registerWithFailure()
                .assertEmailErrorIsShown();
    }

}
