import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;


public class Lab_10_Test_Niepoprawnego_Logowania_Email_DataSource_Test extends SeleniumBaseTest{

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test", "The Email field is not a valid e-mail address."},
                {"admin", "The Email field is not a valid e-mail address."},
                {"@test", "The Email field is not a valid e-mail address."}
        };
    }


    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTest(String wrongEmail, String expectedError) {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail(wrongEmail);
        loginPage.submitLoginWithFailure();


        Assert.assertEquals(loginPage.emailError.getText(), expectedError);

    }

}
