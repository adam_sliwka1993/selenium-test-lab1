import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_9_Test_Niepoprawnego_Logowania_Email extends SeleniumBaseTest {

    @Test
    public void incorrectLoginTestEmailWrong() {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail("test1@test1.com");
        loginPage.typePassword("Test1!");
        loginPage.submitLogin();


        boolean doesErrorExists = false;
        for (int i = 0; i < loginPage.loginErrors.size(); i++) {
            if (loginPage.loginErrors.get(i).getText().equals("Invalid login attempt.")) {
                doesErrorExists = true;
                break;
            }
        }

        Assert.assertTrue(doesErrorExists);
    }

}
